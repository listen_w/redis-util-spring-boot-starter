package cn.org.wangchangjiu.redis.springcache;

import lombok.Data;

import java.util.List;

/**
 * @author wangchangjiu
 * @Classname CacheKeySaver
 * @Description
 * @Date 2024/8/23 17:24
 */
public interface CacheKeySaver {

    void putKey(String name, String key);

    KeyResult getKey(String name);

    @Data
    class KeyResult {

       private List<String> items;

       private boolean last;

       public static KeyResult empty(){
           KeyResult result = new KeyResult();
           result.setLast(true);
           return result;
       }

        public static KeyResult all(List<String> items){
            KeyResult result = new KeyResult();
            result.setItems(items);
            result.setLast(true);
            return result;
        }

    }

}
