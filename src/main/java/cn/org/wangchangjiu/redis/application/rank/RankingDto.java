package cn.org.wangchangjiu.redis.application.rank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author wangchangjiu
 * @Classname RankingDto
 * @Description
 * @Date 2023/12/5 20:22
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RankingDto<T extends RankingDto.RankingItemDto> implements Serializable {


    /**
     *  榜单列表
     */
    private List<T> rankingList;

    /**
     *  当前值，比如当前用户上榜情况
     */
    private T currRanking;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class RankingItemDto {

        /**
         *  唯一ID， 比如用户ID
         */
        private String id;

        /**
         *  排名
         */
        private int ranking;

        /**
         *  榜单值
         */
        private Long val;

        /**
         *  距离上一个差多少
         */
        private Long lastDifferenceVal;

    }

}
