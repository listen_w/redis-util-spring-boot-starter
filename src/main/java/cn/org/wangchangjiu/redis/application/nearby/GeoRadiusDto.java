package cn.org.wangchangjiu.redis.application.nearby;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wangchangjiu
 * @Classname GeoRadiusDto
 * @Description
 * @Date 2023/12/18 21:21
 */
@Data
public class GeoRadiusDto implements Serializable {

    private String name;

    private double longitude;

    private double latitude;

    private double value;

}
