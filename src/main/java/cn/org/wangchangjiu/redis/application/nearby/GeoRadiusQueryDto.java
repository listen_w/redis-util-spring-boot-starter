package cn.org.wangchangjiu.redis.application.nearby;

import lombok.Data;
import org.springframework.data.geo.Metric;

import java.io.Serializable;

/**
 * @author wangchangjiu
 * @Classname GeoRadiusQueryDto
 * @Description
 * @Date 2023/12/18 21:13
 */
@Data
public class GeoRadiusQueryDto implements Serializable {

    private String key;
    private double longitude;
    private double latitude;
    private double value;
    private int limit;
    private int sort;
    private String member;

    private Metric metric;


}
