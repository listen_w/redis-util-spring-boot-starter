package cn.org.wangchangjiu.redis.delay;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @Classname DelayQueueMessageProducer
 * @Description 延迟队列生产者
 * @Date 2022/9/14 10:12
 * @Created by wangchangjiu
 */
@Slf4j
public class DelayQueueMessageProducer {

    private DelayQueue delayQueue;

    private String registerService;

    public DelayQueueMessageProducer(DelayQueue delayQueue, String registerService){
        this.delayQueue = delayQueue;
        this.registerService = registerService;
    }

    public <T> String sendMessage(String topic, T jsonSerializableObject, long delay, TimeUnit timeUnit) {
        String messageId = UUID.randomUUID().toString().replaceAll("-", "");
        RedisDelayMessage message = new RedisDelayMessage(messageId, registerService, topic, JSON.toJSONString(jsonSerializableObject));
        delayQueue.addMessage(message, delay, timeUnit);
        log.info("添加消息：{} 进入 topic ：{} 延迟队列，delay:{} ,timeUnit:{}", JSON.toJSONString(message), topic, delay, timeUnit);
        return messageId;
    }

    public void removeMessage(String topic, String messageId) {
        delayQueue.removeMessage(registerService, topic, messageId);
    }

    public void checkAck(String registerService){
        delayQueue.checkAck(registerService);
    }
}
