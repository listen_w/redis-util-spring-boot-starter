package cn.org.wangchangjiu.redis.delay;

import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.List;

/**
 *  延迟队列 任务执行器
 * @author wangchangjiu
 * @param
 */
public class BatchDelayMessageTaskExecutor implements Runnable {

    private DelayQueue delayQueue;

    private String topic;

    private String registerService;

    private ListenableFutureCallback<List<RedisDelayMessage>> callback;

    public BatchDelayMessageTaskExecutor(DelayQueue delayQueue, String registerService, String topic, ListenableFutureCallback<List<RedisDelayMessage>> callback){
        this.delayQueue = delayQueue;
        this.registerService = registerService;
        this.topic = topic;
        this.callback = callback;
    }


    @Override
    public void run() {
        try {
            List<RedisDelayMessage> messages = delayQueue.getBatchMessages(registerService, topic, 100);
            callback.onSuccess(messages);
        }catch (Exception e){
            callback.onFailure(e);
        }
    }
}
