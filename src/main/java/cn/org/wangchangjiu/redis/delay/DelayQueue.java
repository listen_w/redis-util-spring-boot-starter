package cn.org.wangchangjiu.redis.delay;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Classname DelayQueueMessage
 * @Description
 * @Date 2022/9/14 10:15
 * @Created by wangchangjiu
 */
public interface DelayQueue {

    /**
     *  添加消息
     * @param delayMessage
     * @param delay
     * @param timeUnit
     */
    void addMessage(RedisDelayMessage delayMessage, long delay, TimeUnit timeUnit);

    /**
     *  获取消息
     * @param registerService
     * @param topic
     * @return
     */
    RedisDelayMessage getMessage(String registerService, String topic);

    /**
     *  批量获取消息
     * @param registerService
     * @param topic
     * @param batchSize
     * @return
     */
    List<RedisDelayMessage> getBatchMessages(String registerService, String topic, Integer batchSize);

    /**
     *  移除消息
     * @param registerService
     * @param topic
     * @param messageId
     */
    void removeMessage(String registerService, String topic, String messageId);

    /**
     *  确认消费消息
     * @param registerService
     * @param topic
     * @param messageId
     */
    void ackMessage(String registerService, String topic, String messageId);

    void checkAck(String registerService);


}
