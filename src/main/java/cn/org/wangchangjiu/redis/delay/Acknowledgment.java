package cn.org.wangchangjiu.redis.delay;

/**
 * @author wangchangjiu
 * @Classname Acknowledgment
 * @Description
 * @Date 2024/1/2 19:08
 */
public interface Acknowledgment {

    /**
     *  ack 消息
     */
    void acknowledge();

}
