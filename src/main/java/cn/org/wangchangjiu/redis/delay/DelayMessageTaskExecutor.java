package cn.org.wangchangjiu.redis.delay;

import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 *  延迟队列 任务执行器
 * @author wangchangjiu
 * @param
 */
public class DelayMessageTaskExecutor implements Runnable {

    private DelayQueue delayQueue;

    private String topic;

    private String registerService;

    private ListenableFutureCallback<RedisDelayMessage> callback;

    public DelayMessageTaskExecutor(DelayQueue delayQueue, String registerService, String topic, ListenableFutureCallback<RedisDelayMessage> callback){
        this.delayQueue = delayQueue;
        this.registerService = registerService;
        this.topic = topic;
        this.callback = callback;
    }


    @Override
    public void run() {
        try {
            RedisDelayMessage message = delayQueue.getMessage(registerService, topic);
            callback.onSuccess(message);
        }catch (Exception e){
            callback.onFailure(e);
        }
    }
}
