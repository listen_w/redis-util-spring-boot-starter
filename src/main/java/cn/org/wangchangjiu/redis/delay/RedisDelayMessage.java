package cn.org.wangchangjiu.redis.delay;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Classname RedisDelayMssage
 * @Description
 * @Date 2022/10/14 16:30
 * @Created by wangchangjiu
 */
@Data
public class RedisDelayMessage<T> implements Serializable {

    private String messageId;

    private String registerService;

    private String topic;

    private String messageBody;

    private Long expiredTime;

    private T data;

    public RedisDelayMessage(String messageId, String registerService, String topic, String messageBody){
        this.messageId = messageId;
        this.registerService = registerService;
        this.topic = topic;
        this.messageBody = messageBody;
    }

}
