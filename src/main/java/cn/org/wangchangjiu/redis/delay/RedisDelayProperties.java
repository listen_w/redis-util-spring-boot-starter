package cn.org.wangchangjiu.redis.delay;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author wangchangjiu
 * @Classname RedissonDelayProperties
 * @Description
 * @Date 2022/9/15 9:51
 */
@Data
@ConfigurationProperties(prefix = "redis.util.delay")
public class RedisDelayProperties {

    private String registerService = "other";

    private Long boosThreadSleep = 500L;

    private Long backThreadSleep = 5000L;

    private Long ackDelay = 5000L;

    private Long redeliveryTtl = 10L;

}
